
;+
; :Description: A code to calculate TC Greenness index.
;
; :INPUT:  in_tile_path - a path to the folder with S2 L2A bands saved as tiffs at 10m resolution (all bands) passed from the S2_Idaho_Processing_start_v2.pro
;          tile - a name of a tile to be processed passed from the S2_Idaho_Processing_start_v2.pro
;
; :DEPENDENCE: S2_Idaho_Processing_start_v2.pro
;
;
; :AUTHOR: Katarzyna Ewa Lewińska
; :DATE: 9 January 2018
;-

FUNCTION S2_Idaho_idx_cal_GRE_v1, in_tile_path, tile
  ;PRO S2_Idaho_idx_cal_GRE_v1


  t0 = SYSTIME()
  print,' GREENEESS calculation initiated: ' +t0
  
  in_dates_folders = FILE_SEARCH(in_tile_path, tile+ '_2017????T??????_?', /TEST_DIRECTORY)
  
  FOR i=0, N_ELEMENTS(in_dates_folders)-1 DO BEGIN
    b2 = FILE_SEARCH(in_dates_folders[i],'\translate\*B02.tif')
    b3 = FILE_SEARCH(in_dates_folders[i],'\translate\*B03.tif')
    b4 = FILE_SEARCH(in_dates_folders[i],'\translate\*B04.tif')
    b8 = FILE_SEARCH(in_dates_folders[i],'\translate\*B08.tif')
    b11 = FILE_SEARCH(in_dates_folders[i],'\translate\*B11.tif')
    b12 = FILE_SEARCH(in_dates_folders[i],'\translate\*B12.tif')
  
    out_name = in_dates_folders[i] + '\translate\' + FILE_BASENAME(b4,'B04.tif')+ 'GRE.tif'
  
  
    quer = QUERY_TIFF(b4, GEOTIFF=geo)
    
    im_b2 = READ_TIFF(b2, INTERLEAVE=0)
    im_b3 = READ_TIFF(b3, INTERLEAVE=0)
    im_b4 = READ_TIFF(b4, INTERLEAVE=0)
    im_b8 = READ_TIFF(b8, INTERLEAVE=0)
    im_b11 = READ_TIFF(b11, INTERLEAVE=0)
    im_b12 = READ_TIFF(b12, INTERLEAVE=0)
    
  
    GRE_image = (-0.2848*im_b2)-(0.2435*im_b3)-(0.5436*im_b4)+(0.7243*im_b8)+(0.0840*im_b11)-(0.1800*im_b12)
    
    WRITE_TIFF, out_name, GRE_image, GEOTIFF=geo, DESCRIPTION='GRE', /FLOAT, COMPRESSION=2
    
    print, '  Greeneess calculated for: ' + FILE_BASENAME(b4,'B04.tif')

  ENDFOR
  
  t0 = SYSTIME()
  print,' GREENEESS calculation completed: ' +t0
  
  return, out_name

END