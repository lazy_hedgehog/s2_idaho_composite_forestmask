;+
; Version: 1.1
;
; :Description:
;    Compositing of Sentinel-2 data (10m ressolution) given in the specific file structure into one cloud-free scene per tile
;
;   INPUT:  in_path – a targeted input directory with desired tile and folder structure
;           out_path – a targeted output directory for composites (L1 data)
;           fm_path – a targeted output directory for forest mask (L2 data)
;           start_time – a first day of a time window used to create a synthetic composite, a long number defined as YYYYMMDD
;           end_time – a last day of a time window used to create a synthetic composite a long number defined as YYYYMMDD
;           band_list – a string vector where all bands to be composited are specified. In order to generate Forest Mask, band 4, 8, 11 and 12 are essential.
;           band_list = [‘B04.’, ‘B08.’, ’B11.’, ‘B12.’]
;
;   HISTORY:
;
; :AUTHOR: Katarzyna Ewa Lewińska
; :YEAR: 2018
;-

PRO S2_Idaho_Processing_start_v2


  ;;;catch error
  !error_state.code = 0
  CATCH, error
  if (error ne 0) then begin
    ok = DIALOG_MESSAGE(!error_state.msg, /CANCEL)
    if (STRUPCASE(ok) eq 'CANCEL') then STOP; RETURN
  endif

  ;;;;;;;;;;;;;;;;;;;;;

  in_path = 'L:\delivery\000058_IdahoForestryGroup\01_SourceData\S2\01_tiles' ;;11TPM_20170701_B
  out_path = 'L:\delivery\000058_IdahoForestryGroup\04_DataProducts\L1\Composites'
  fm_path = 'L:\delivery\000058_IdahoForestryGroup\04_DataProducts\L2\Forest_mask'

  start_time = 20170701
  end_time = 20170831
  
    band_list = ['B02.', 'B03.', 'B04.', 'B05.', 'B06.', 'B07.', 'B08.', 'B8A.', 'B11.', 'B12.']
;  band_list = ['B04.', 'B08.', 'B11.', 'B12.']

  ;;;;;;;;;;;;;;;;;;;;;

   JOURNAL, out_path+'\S2_Composite_processing_log2.csv'

  in_tiles_folders = FILE_SEARCH(in_path, '1????', /TEST_DIRECTORY)
  in_tiles_folders = in_tiles_folders[WHERE(STRLEN(in_tiles_folders) EQ (STRLEN(in_path) + 6))]

  FOR t=0, N_ELEMENTS(in_tiles_folders)-1 DO BEGIN

    t1 = SYSTIME()

    in_tile_path = in_tiles_folders[t]
    tile = STRMID(in_tile_path,4,5, /REVERSE_OFFSET)

    print, 'processint tile ' + tile
    print, 'processing iniciated: ' + t1

    ;;; calculate indices
    gre_file = S2_Idaho_idx_cal_GRE_v1(in_tile_path, tile)

    ;;; calculate synthetic composite
    comp_folder = S2_Idaho_syn_comp_v4(in_tile_path, out_path, tile, start_time, end_time, band_list)

;    comp_folder = 'L:\delivery\000058_IdahoForestryGroup\04_DataProducts\L1\Composites\11TPM'
    ;;; calculate ndvi for the composite
    out_ndvi = S2_Idaho_idx_cal_NDVI_v1(comp_folder)

    ;;; calculate forest mask
    fm = S2_Idaho_forest_mask_v2(comp_folder, fm_path)
    
    ;;; delete Greeneess 
    gre_list = FILE_SEARCH(in_path, '*\translate\*GRE.tif')
    FOR z=0, N_ELEMENTS(gre_list)-1 DO FILE_DELETE, gre_list[z]
    print, 'All Greenness files have been deleted'
    
    t2 = SYSTIME()                                                    ; end time
    print, 'processing completed: ' + t2                              ; print end time
    print, ''

  ENDFOR

  JOURNAL


END