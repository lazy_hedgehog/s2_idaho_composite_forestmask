
;+
; :Description: A code calculates NDVI using Bnad 4 and Band 8.
;
; :INPUT:  in_path – a targeted input directory for composites (L1 data) passed from the S2_Idaho_Processing_start_v2.pro
;
; :DEPENDENCE: S2_Idaho_Processing_start_v2.pro
;
;
; :AUTHOR: Katarzyna Ewa Lewińska
; :DATE: 6 December 2017
;-

FUNCTION S2_Idaho_idx_cal_NDVI_v1, in_path
  ;PRO S2_Idaho_idx_cal_NDVI_v1
  
  t0 = SYSTIME()
  print,' NDVI calculation initiated: ' +t0

  b4 = FILE_SEARCH(in_path,'\*B04.tif')
  b8 = FILE_SEARCH(in_path,'\*B08.tif')

  out_name = in_path + '\' + FILE_BASENAME(b4,'B04.tif')+ 'NDVI.tif'

  im_b4 = READ_TIFF(b4, INTERLEAVE=0)
  im_b8 = READ_TIFF(b8, INTERLEAVE=0)
  
  quer = QUERY_TIFF(b4, GEOTIFF=geo)


  NDVI_image = FLOAT(FIX(im_b8 - im_b4))/FLOAT(FIX(im_b8 + im_b4))

  WRITE_TIFF, out_name, NDVI_image, GEOTIFF=geo, DESCRIPTION='NDVI', /FLOAT, COMPRESSION=2

  t0 = SYSTIME()
  print,' NDVI calculation completed: ' +t0

  RETURN, out_name


END