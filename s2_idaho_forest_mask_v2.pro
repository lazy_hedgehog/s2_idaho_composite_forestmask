
;+
; :Description: A code to derive a forest mask for S2 data, where each band is a separate tiff file.
;               The code uses NDVI, Band 8 and Band 12.
;               the approach is an implementation of the forest classification developed by Zhu et al., (2012).
;               doi:10.1016/j.rse.2011.10.030
;
; :INPUT:  in_path – a targeted output directory for composites (L1 data) passed from the S2_Idaho_Processing_start_v2.pro
;          fm_path – a targeted output directory for forest mask (L2 data) passed from the S2_Idaho_Processing_start_v2.pro
;
; :DEPENDENCE: S2_processing_start.pro
;
;
; :AUTHOR: Katarzyna Ewa Lewińska
; :DATE: 8 January 2018
;-

FUNCTION S2_Idaho_forest_mask_v2, in_path, fm_path
  ;PRO S2_Idaho_Forest_maks_v1

  t0 = SYSTIME()
  print,'   FOREST MASK calculation initiated: ' +t0


  ndvi = FILE_SEARCH(in_path, '\*NDVI.tif')
  b11 = FILE_SEARCH(in_path,'\*B11.tif')
  b12 = FILE_SEARCH(in_path,'\*B12.tif')

  out_name = fm_path + '\' + FILE_BASENAME(b11,'B11.tif')+ 'FM.tif'
  
  im_b11 = READ_TIFF(b11, INTERLEAVE=0)
  im_b12 = READ_TIFF(b12, INTERLEAVE=0)
  im_ndvi = READ_TIFF(ndvi, INTERLEAVE=0)
  
  quer = QUERY_TIFF(b11, GEOTIFF=geo)
  
  fm_image = BYTARR((SIZE(im_b11))[1],(SIZE(im_b11))[1])

  fm_image[WHERE(im_ndvi GE 0.6 AND im_b12 LT 1000 AND im_b11 LT 2000)]=1
  
  WRITE_TIFF, out_name, fm_image, DESCRIPTION='Forest Mask', GEOTIFF=geo, /SHORT, COMPRESSION=1

  t0 = SYSTIME()
  print,'   FOREST MASK calculation completed: ' +t0

  RETURN, out_name


END