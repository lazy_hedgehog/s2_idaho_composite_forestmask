
;+
; :Description: A code to derive a synthetic, cloud free composite for Senthinel-2 data. Works on a tile-basis, considering all scenes available for a given tile.
;               The code is based on a premis that the best quality pixel is the one with the highest Greenness value.
;               The algorithm assumes limited RAM resources, hence instead of keeping all essential datasets and variables in memory, 
;               refers to the datasets stored on drive. Consequently, computation time depends on a data transfer capacity. 
;
; :INPUT:  in_path – a targeted input directory with desired tile and folder structure passed from the S2_Idaho_Processing_start_v2.pro
;          out_path – a targeted output directory for composites (L1 data) passed from the S2_Idaho_Processing_start_v2.pro
;          tile - a name of a tile to be processed passed from the S2_Idaho_Processing_start_v2.pro
;          start_time – a first day of a time window used to create a synthetic composite, a long number defined as YYYYMMDD, passed from the S2_Idaho_Processing_start_v2.pro
;          end_time – a last day of a time window used to create a synthetic composite a long number defined as YYYYMMDD, passed from the S2_Idaho_Processing_start_v2.pro
;          band_list – a string vector where all bands to be composited are specified. In order to generate Forest Mask, band 4, 8, 11 and 12 are essential.
;                       band_list = [‘B04.’, ‘B08.’, ’B11.’, ‘B12.’]. passed from the S2_Idaho_Processing_start_v2.pro
;
; :DEPENDENCE: S2_Idaho_Processing_start_v2.pro
;
;
; :AUTHOR: Katarzyna Ewa Lewińska
; :DATE: 11 January 2018
;-


FUNCTION S2_Idaho_syn_comp_v4, in_path, out_path, tile, start_time, end_time, band_list
  ;PRO S2_Idaho_syn_comp_v4


  t0 = SYSTIME()
  print,' Compositing initiated: ' +t0
  ;;;;;;;;;;;;;;;;;;;;;

  out_folder = out_path +'\'+ tile
  folder_test = FILE_TEST(out_folder , /DIRECTORY )
  IF folder_test EQ 0 THEN FILE_MKDIR, out_folder

  ;;;;;;;;;;;;;;;;;;;;;


  gre_list = FILE_SEARCH(in_path, '*\translate\*GRE.tif')

  gre_list = gre_list[WHERE(STRMID(FILE_BASENAME(gre_list[*]),6,8) GE start_time AND STRMID(FILE_BASENAME(gre_list[*]),6,8) LE end_time)]

  image_list = STRARR(N_ELEMENTS(gre_list))

   print,'    Number of files to be used for a composite: ' +N_ELEMENTS(gre_list)

  ;;; create variable names
  FOR n=0, N_ELEMENTS(gre_list)-1 DO BEGIN
    name='band_' + STRTRIM(STRING(n),2)
    image_list[n]=name

  ENDFOR

  ;;;doy
  doy_list = STRMID(FILE_BASENAME(gre_list),6,8)
  FOR d=0, N_ELEMENTS(doy_list)-1 DO doy_list[d] = JULDAY(STRMID(doy_list[d],4,2), STRMID(doy_list[d],6,2), STRMID(doy_list[d],0,4))-JULDAY(01,01,STRMID(doy_list[d],0,4))+1

  ;;; core name
  comp_name = STRTRIM(tile,2) + '_' + STRMID(FILE_BASENAME(gre_list[0]),6,8) + '_' + STRMID(FILE_BASENAME(gre_list[-1]),6,8) + '_'

  mm = !NULL

  ;;; OPEN
;    ;;; 1. RAM DEMANDING SOLUTION
;    FOR n=0, N_ELEMENTS(gre_list)-1 DO BEGIN
;      (scope_varfetch(image_list[n], /enter, level=0))=READ_TIFF(gre_list[n], INTERLEAVE=0)
;      (scope_varfetch(image_list[n], /enter, level=0))[WHERE(~FINITE(scope_varfetch(image_list[n], /enter, level=0)) OR (scope_varfetch(image_list[n], /enter, level=0)) EQ 0.0, /NULL)]= (-10000)
;      mm = [[[mm]],[[(scope_varfetch(image_list[n], /enter, level=0))]]]
;    ENDFOR
;    quer = QUERY_TIFF(gre_list[0], GEOTIFF=geo)
;  
;    mmax = MAX(mm, DIMENSION=3)
;    
;    mm=0 ; clear memory
;    
;    idx_image = BYTARR((SIZE(band_0))[1],(SIZE(band_0))[1])
;    doy_image = BYTARR((SIZE(band_0))[1],(SIZE(band_0))[1])
;    
;    FOR n=0, N_ELEMENTS(gre_list)-1 DO BEGIN
;     (scope_varfetch(image_list[n], /enter, level=0)) = WHERE((scope_varfetch(image_list[n], /enter, level=0)) EQ mmax)
;    
;    idx_image[(scope_varfetch(image_list[n], /enter, level=0))] = n
;    
;    IF STRMID(FILE_BASENAME(gre_list[n]),8,1,/REVERSE_OFFSET) EQ 'A' THEN doy_image[(scope_varfetch(image_list[n], /enter, level=0))] = doy_list[n]         ;S2A
;    IF STRMID(FILE_BASENAME(gre_list[n]),8,1,/REVERSE_OFFSET) EQ 'B' THEN doy_image[(scope_varfetch(image_list[n], /enter, level=0))] = 1000 + doy_list[n]   ;S2B
;    ENDFOR

  ;;; 2. DATA TRANSFER DEMANDING SOLUTION
    FOR n=0, N_ELEMENTS(gre_list)-1 DO BEGIN
      band_0=READ_TIFF(gre_list[n], INTERLEAVE=0)
      band_0[WHERE(~FINITE(band_0) OR band_0 EQ 0.0, /NULL)]= (-10000)
      mm = [[[mm]],[[band_0]]]
    ENDFOR
    quer = QUERY_TIFF(gre_list[0], GEOTIFF=geo)
  
    mmax = MAX(mm, DIMENSION=3)
  
    mm=0 ; clear memory
  
    idx_image = BYTARR((SIZE(band_0))[1],(SIZE(band_0))[1])
    doy_image = BYTARR((SIZE(band_0))[1],(SIZE(band_0))[1])
  
    FOR n=0, N_ELEMENTS(gre_list)-1 DO BEGIN
      band_0=READ_TIFF(gre_list[n], INTERLEAVE=0)
      band_0 = WHERE(band_0 EQ mmax)
  
      idx_image[band_0] = n
  
      IF STRMID(FILE_BASENAME(gre_list[n]),8,1,/REVERSE_OFFSET) EQ 'A' THEN doy_image[band_0] = doy_list[n]          ;S2A
      IF STRMID(FILE_BASENAME(gre_list[n]),8,1,/REVERSE_OFFSET) EQ 'B' THEN doy_image[band_0] = 1000 + doy_list[n]   ;S2B
    ENDFOR


  out_file = out_folder+ '\'+ comp_name + 'IDX.tif'
  out_doy_file = out_folder+ '\'+ comp_name + 'DOY.tif'

  WRITE_TIFF, out_file, idx_image, GEOTIFF=geo, /SHORT, COMPRESSION=1
  WRITE_TIFF, out_doy_file, doy_image, GEOTIFF=geo, /SHORT, COMPRESSION=1


  ;;; iterate through bands and create composites
  FOR j=0, N_ELEMENTS(band_list)-1 DO BEGIN

    in_list = FILE_SEARCH(in_path, '\'+STRTRIM(tile,2)+'*\translate\*' + STRTRIM(band_list[j],2) + 'tif')
    in_list = in_list[WHERE(STRMID(FILE_BASENAME(in_list[*]),6,8) GE start_time AND STRMID(FILE_BASENAME(in_list[*]),6,8) LE end_time)]

    band_image = INTARR((SIZE(idx_image))[1],(SIZE(idx_image))[1])

    FOR n=0, N_ELEMENTS(in_list)-1 DO BEGIN
      band = READ_TIFF(in_list[n], INTERLEAVE=0)
      band_image[WHERE(idx_image EQ n)] = band[WHERE(idx_image EQ n)]
    ENDFOR

    out_band_file = out_folder+ '\'+ comp_name + STRTRIM(band_list[j],2)+ 'tif'

    WRITE_TIFF, out_band_file, band_image, GEOTIFF=geo, /SHORT, COMPRESSION=1

  ENDFOR

  t0 = SYSTIME()
  print,' Compositing completed ' +t0

  return, out_folder


END