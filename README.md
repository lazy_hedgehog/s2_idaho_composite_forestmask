
# S2_Idaho_compositing
# IDL project version
# Version 1.1

 
S2_Idaho_compositing is an IDL project comprising scripts generating an ‘idealistic’ tile-image with no clouds, 
shadows nor no-data pixels. Subsequently, a forest mask is generated for each tile using the composited, cloud-free image. 
The process runs on S2 data stored in the following data structure (in_path parameter in the executing script should point 
to the ‘S2’ folder level):
 
Where the ‘translate’ folder comprises S2 bands after sen2cor processing (TIFF format). 
 
If within the S2 directory (or a folder with any other name comprising the data) multiple tile related folders are present, 
the algorithm will run consecutively over all tiles. 
The algorithm is sensitive to any change in the default naming convention of the input files or file structure! 
Scenes does not need to be clouds and shadow masked, as the algorithm can roughly identify those pixels.  
Installation and running
The algorithm is disseminated as an IDL project. It requires only IDL routines, taking no ENVI license. If needed, 
it can be exported to a stand-alone version executed on an IDL VM.
From the IDL GUI create a new project, importing the S2_Idaho_compositing project from the folder.
 
# The algorithm is launched by running the S2_Idaho_Processing_start_v2.pro script.
The following parameters need to be specified in the S2_Idaho_Processing_start_v2.pro code:

•	in_path – a targeted input directory with desired tile and folder structure

•	out_path – a targeted output directory for composites (L1 data)

•	fm_path – a targeted output directory for tree cover mask (L2 data)

•	start_time – a first day of a time window used to create a synthetic composite, a long number defined as YYYYMMDD

•	end_time – a last day of a time window used to create a synthetic composite a long number defined as YYYYMMDD

•	band_list – a string vector where all bands to be composited are specified. In order to generate Tree Cover Mask, band 4, 8, 11 and 12 are essential. 
band_list = [‘B04.’, ‘B08.’, ’B11.’, ‘B12.’]
 
 
The flowing outputs are generated:

•	temporal Greenness index calculated for each scene within the tile, and saved within the file structure of the original data. Those datasets are automatically deleted on process is through;

•	DOY (day of the year) and IDX (index) images, generated for each tile and located within the L1 file structure, specified with the out_path. IDX is essential for calculation of a synthetic composite;

•	Synthetic composites for bands specified with the band_list parameter. Resulting bands are saved within the L1 file structure specified with the out_path;

•	NDVI calculated using Band 4 and Band 8 synthetic composites. NDVI band is saved in the L1 file structure specified with the out_path;

•	Tree Cover Mask (FM) is saved in the L2 file structure specified with the fm_path;

•	A log file summing up  the processing time on every step of computation. It is saved in a folder specified in the out_path variable. The log file is always called: S2_Composite_processing_log.txt. 

 
Generated files at the composite level (L1) have the following name convention:

TTTTT_YYYYMMDD_yyyymmdd_XXX.tif

Where:
TTTTT – tile name/number (e.g. 11TPM)

YYYYMMDD – a date of the first scene within the inspected time window used in the compositing process;

yyyymmdd – a date of the last scene within the inspected time window used in the compositing process; 

XXX – index.name of the band, e.g. IDX, DOY, NDVI, FM, B04 etc. 

# Methodology
The algorithm works based on rather simple assumptions used in the compositing and forest detection.
The most efficient compositing approaches , ,  (Ramoino and Tutunaru, 2017) are based on pixel sorting, 
where each pixel of a resulting composite is the highest quality pixel identified within a given time period/time series 
of data. Following this assumption, this algorithm seeks for the greenness pixel within a time series. 
The tree cover mask is generated based on findings of Zhu et al. (2012) who demonstrated that forest vegetation can be 
successfully detected using NDVI and SWIR bands.

# Benchmarking:
The algorithm assumes limited RAM resources, hence instead of keeping all essential datasets and variables 
in memory, refers to the datasets stored on drive. Consequently, computation time depends on a data transfer capacity. 
The test run was performed using a VM with 16 CPU (2.3GHz) and 16GB RAM.
This environment was sufficient to create synthetic composites for 4 bands (b4, b8, b11 and b12), calculate NDVI 
and finally tree cover mask for a single tile based on 23 consecutive scenes. The process run for: 1h. 

# References:
Ramoino, F., Tutunaru, F., 2017. Ten-Meter Sentinel-2A Cloud-Free Composite—Southern Africa 2016. Remote Sens. 9. doi:10.3390/rs9070652

Zhu, Z., Woodcock, C.E., Olofsson, P., 2012. Continuous monitoring of forest disturbance using all available Landsat imagery. Remote Sens. Environ. 122, 75–91. doi:10.1016/j.rse.2011.10.030

